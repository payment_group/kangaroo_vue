import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import './plugin'
import 'normalize.css/normalize.css'


Vue.config.productionTip = false


new Vue({
    render: h => h(App),
    store,
    router
}).$mount('#app')
