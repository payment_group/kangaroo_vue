import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from "@/views/Layout";
import Welcome from "@/components/Welcome";
import WaitJobs from "@/components/WaitJobs";
import FinishJobs from "@/components/FinishJobs";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        component: Layout,
        redirect: '/welcome',
        children: [
            {path: '/welcome',component: Welcome},
            {path:'/waitJobs',component: WaitJobs},
            {path:'/finishJobs',component: FinishJobs}
        ]
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
